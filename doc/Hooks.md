# Hooks
ErrorSanitizer currently supports hooks for:

*  int [access](https://man7.org/linux/man-pages/man2/access.2.html)(const char *pathname, int mode);
*  void* [calloc](https://en.cppreference.com/w/c/memory/calloc)( size_t num, size_t size );
*  int [chmod](https://man7.org/linux/man-pages/man2/chmod.2.html)(const char *pathname, mode_t mode);
*  int [faccessat](https://man7.org/linux/man-pages/man2/access.2.html)(int dirfd, const char *pathname, int mode, int flags);
*  int [fchmod](https://man7.org/linux/man-pages/man2/chmod.2.html)(int fd, mode_t mode);
*  int [fclose](https://en.cppreference.com/w/c/io/fclose)( FILE *stream );
*  int [fgetpos](https://man7.org/linux/man-pages/man3/fgetpos.3p.html)(FILE *restrict stream, fpos_t *restrict pos);
*  FILE* [fopen](https://en.cppreference.com/w/c/io/fopen)( const char *filename, const char *mode );
*  int [fputs](https://en.cppreference.com/w/c/io/fputs)( const char *restrict str, FILE *restrict stream );
*  FILE* [freopen](https://man7.org/linux/man-pages/man3/freopen.3p.html)(const char *restrict pathname, const char *restrict mode, FILE *restrict stream);
*  int [fseek](https://man7.org/linux/man-pages/man3/fseek.3.html)(FILE *stream, long offset, int whence);
*  int [fsetpos](https://man7.org/linux/man-pages/man3/fsetpos.3p.html)(FILE *stream, const fpos_t *pos);
*  long [ftell](https://en.cppreference.com/w/c/io/ftell)( FILE *stream );
*  off_t [ftello](https://man7.org/linux/man-pages/man3/ftello.3p.html)(FILE *stream);
*  void* [malloc](https://en.cppreference.com/w/c/memory/malloc)( size_t size );
*  void* [realloc](https://en.cppreference.com/w/c/memory/realloc)( void *ptr, size_t new_size );
*  void [rewind](https://en.cppreference.com/w/c/io/rewind)( FILE *stream );
*  void [setbuf](https://man7.org/linux/man-pages/man3/setbuf.3.html)(FILE *stream, char *buf);
*  int [setvbuf](https://man7.org/linux/man-pages/man3/setbuf.3.html)(FILE *stream, char *buf, int mode, size_t size);
*  char* [strdup](https://man7.org/linux/man-pages/man3/strdup.3.html)(const char *s);
*  char* [strndup](https://man7.org/linux/man-pages/man3/strdup.3.html)(const char *s, size_t n);
*  FILE* [tmpfile](https://en.cppreference.com/w/c/io/tmpfile)(void);
*  char* [tmpnam](https://en.cppreference.com/w/c/io/tmpnam)( char *filename );
